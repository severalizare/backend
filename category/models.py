from django.db import models

# Create your models here.
class Category(models.Model):
	title = models.CharField(max_length=32, verbose_name='عنوان دسته')
	slug = models.SlugField(unique=True, verbose_name="آدرس دسته", help_text="باید منحصر به فرد باشد.")
	status = models.BooleanField(verbose_name='نمایش داده شود؟')
	position = models.PositiveIntegerField(default=0, verbose_name='جایگاه', help_text="بزرگترین عدد در ابتدا نمایش داده می شود.")
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	class Meta:
		ordering = ('-position', 'created')
		verbose_name = "دسته بندی"
		verbose_name_plural = "دسته بندی ها"

	def __str__(self):
		return self.title