from django.contrib import admin
from .models import Article

# Register your models here.
class ArticleAdmin(admin.ModelAdmin):
	search_fields = ('title', 'content')
	list_filter = ('category', 'publish', 'author')
	list_display = ('type_article', 'title', 'slug', 'description', 'category', 'author', 'image_tag', 'publish', 'status',)
admin.site.register(Article, ArticleAdmin)