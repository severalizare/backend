from django import template
from article.models import Article
from category.models import Category

register = template.Library()

@register.inclusion_tag('article/parts/news_area.html')
def news_area_last_3(slug):
	articles = Article.objects.filter(category__slug=slug, status=True)[:3]
	category = Category.objects.get(slug=slug, status=True)
	return {'articles': articles, 'category': category}