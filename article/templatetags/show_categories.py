from django import template
from category.models import Category

register = template.Library()

@register.inclusion_tag('article/parts/show_categories.html')
def show_categories():
	categories = Category.objects.filter(status=True)
	return {'categories': categories,}