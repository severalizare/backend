from django.shortcuts import render, get_object_or_404
from .models import Article
from category.models import Category
from django.core.paginator import Paginator


# Create your views here.
def home(request):
	events = Article.objects.filter(category__slug='events', status=True)[:3]
	return render(request, 'article/home.html', {'events': events,})

def category(request, slug):
	category = get_object_or_404(Category, status=True, slug=slug)
	articles_list = category.article_set.filter(status=True)
	page = request.GET.get('page', 1)
	paginator = Paginator(articles_list, 10)
	try:
		articles = paginator.page(page)
	except PageNotAnInteger:
		articles = paginator.page(1)
	except EmptyPage:
		articles = paginator.page(paginator.num_pages)
	context = {
		'category': category,
		'articles': articles,
	}
	return render(request, 'article/category.html', context)

def post(request, slug):
	context = {
		'article': get_object_or_404(Article, status=True, slug=slug, type_article='post'),
	}
	return render(request, 'article/single.html', context)

def page(request, slug):
	context = {
		'article': get_object_or_404(Article, status=True, slug=slug, type_article='page'),
	}
	return render(request, 'article/single.html', context)